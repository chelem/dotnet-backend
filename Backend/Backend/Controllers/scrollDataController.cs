﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.src;

namespace Backend.Controllers
{
    public class scrollDataController : Controller
    {
        [Route("api/scrollData")]
        public List<scrollData> Index()
        {
             dbContext context = HttpContext.RequestServices.GetService(typeof(Backend.src.dbContext)) as dbContext;

            return context.GetScrollData();
        }
    }
}