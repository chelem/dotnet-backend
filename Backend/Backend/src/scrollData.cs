﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.src
{
    public class scrollData
    {
        private dbContext context;
        public string name { get; set; }
        public string scrollId { get; set; }
        public string scrollDataId { get; set; }
        public string scrollVersionId { get; set; }
        public string userId { get; set; }
        public string numberOfVersions { get; set; }
        public string scroll_version_ids { get; set; }
        public string thumbnails { get; set; }
        public string imaged_fragments { get; set; } 

    }
}
