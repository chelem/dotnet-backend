﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;


namespace Backend.src
{
    public class dbContext
    {
        public string ConnectionString { get; set; }

        public dbContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionString);
        }
        public List<scrollData> GetScrollData()
        {
            List<scrollData> list = new List<scrollData>();
            string query = @"SELECT scroll_data.name, 
                            scroll_data_owner.scroll_version_id, 
	                        scroll_version.user_id,
	                        COUNT(DISTINCT scroll_version.scroll_version_id) AS number_of_versions,
                            GROUP_CONCAT(CONCAT('{scroll_version_id:', scroll_version.scroll_version_id, '}')) scroll_version_ids,
	                        GROUP_CONCAT(CONCAT('{proxy:', image_urls.proxy, ', url:', image_urls.url, ', fileName:', SQE_image.filename, '}')) thumbnails,
	                        COUNT(DISTINCT SQE_image.sqe_image_id) as imaged_fragments
                            FROM scroll_data
                            JOIN scroll_data_owner USING(scroll_data_id)
                            JOIN scroll_version USING(scroll_version_id)
                            LEFT JOIN edition_catalog ON edition_catalog.scroll_id = scroll_data.scroll_id
                            AND edition_catalog.edition_side = 0
                            LEFT JOIN image_to_edition_catalog USING(edition_catalog_id)
                            LEFT JOIN SQE_image ON SQE_image.image_catalog_id = image_to_edition_catalog.image_catalog_id
                            AND SQE_image.type = 0
                            LEFT JOIN image_urls USING(image_urls_id)
                            WHERE scroll_version.user_id = 1
                            GROUP BY scroll_data.scroll_id";
            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(query, conn);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        list.Add(new scrollData()
                        {
                            name = reader["name"].ToString(),
                            scrollVersionId = reader["scroll_version_id"].ToString(),
                            userId = reader["user_id"].ToString(),
                            numberOfVersions = reader["number_of_versions"].ToString(),
                            scroll_version_ids = reader["scroll_version_ids"].ToString(),
                            thumbnails = reader["thumbnails"].ToString(),
                            imaged_fragments = reader["imaged_fragments"].ToString() 
                            /*name = reader["name"].ToString(),
                            scrollId = reader["scroll_id"].ToString(),
                            scrollDataId = reader["scroll_data_id"].ToString()**/

                        });
                    }
                }
            }
            return list;
        }
    }
}
